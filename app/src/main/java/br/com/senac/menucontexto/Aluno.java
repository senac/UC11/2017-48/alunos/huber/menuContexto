package br.com.senac.menucontexto;

public class Aluno {

    private String nome ;
    private String telefone ;
    private String email;
    private String site  ;

    public Aluno() {
    }

    public Aluno(String nome, String telefone, String email, String site) {
        this.nome = nome;
        this.telefone = telefone;
        this.email = email;
        this.site = site;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    @Override
    public String toString() {
        return this.nome;
    }
}
